from pathlib import Path

import numpy as np

from src.day_11 import Item, Monkey, solve


class TestFromMonkey:
    def test_factory(self):
        content = """Monkey 0:
        Starting items: 79, 98
        Operation: new = old * 19
        Test: divisible by 23
        If true: throw to monkey 2
        If false: throw to monkey 3
        """
        monkey = Monkey.from_txt(content, True)
        round = monkey.round()
        np.testing.assert_array_equal(round, [(500, 3), (620, 3)])
        assert monkey.inspected == 2
        assert not len(monkey.items)

        monkey.items.append(65)
        monkey.round()
        assert monkey.inspected == 3

    def test_round_square(self):
        m = Monkey(
            [13, 12], op="square", argument=None, decision=(13, 4, 5), divide=False
        )
        out = m.round()
        np.testing.assert_array_equal(out, [[169, 4], [144, 5]])

    def test_round_square_item(self):
        m = Monkey(
            [Item(13, (13, 17)), Item(12, (13, 17))],
            op="square",
            argument=None,
            decision=(13, 4, 5),
            divide=False,
        )
        out = m.round()
        receivers = [x[1] for x in out]
        np.testing.assert_array_equal(receivers, [4, 5])
        assert out[0][0].rests[13] == 0
        assert out[0][0].rests[17] == 16
        assert out[1][0].rests[13] == 1
        assert out[1][0].rests[17] == 8

    def test_round_empty(self):
        m = Monkey([], op="square", argument=None, decision=(23, 1, 2), divide=False)
        out = m.round()
        assert not len(out)
        assert m.inspected == 0

    def test_round_item(self):
        monkey = Monkey(
            [Item(79, (23,)), Item(98, (23,)), Item(23, (23,))],
            op="mul",
            argument=19,
            decision=(23, 2, 3),
            divide=False,
        )
        round = monkey.round()
        receivers = [x[1] for x in round]
        np.testing.assert_array_equal(receivers, [3, 3, 2])
        assert monkey.inspected == 3


def test_solve():
    fn = Path(__file__).parent / "fixtures" / "day_11.txt"
    out = solve(fn, 20, True)
    assert out == 10605


def test_solve_round_one_no_divide():
    fn = Path(__file__).parent / "fixtures" / "day_11.txt"
    out = solve(fn, 1, False)
    assert out == 24


def test_solve_round_twenty_no_divide():
    fn = Path(__file__).parent / "fixtures" / "day_11.txt"
    out = solve(fn, 20, False)
    assert out == 103 * 99


def test_solve_round_thousand_no_divide():
    fn = Path(__file__).parent / "fixtures" / "day_11.txt"
    out = solve(fn, 1000, False)
    assert out == 5204 * 5192


def test_solve_part2():
    fn = Path(__file__).parent / "fixtures" / "day_11.txt"
    out = solve(fn, 10000, False)
    assert out == 2713310158
