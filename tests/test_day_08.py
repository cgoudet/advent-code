from pathlib import Path

from src.day_08 import scenic_view, solve


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_08.txt"
    out = solve(fn)
    assert out == 21


def test_scenic_view():
    fn = Path(__file__).parent / "fixtures" / "day_08.txt"
    out = scenic_view(fn)
    assert out == 8
