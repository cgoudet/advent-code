from pathlib import Path

from src.day_01 import solve


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_01.txt"
    out = solve(fn, 1)
    assert out == ((3,), 24000)


def test_top_3_elves():
    fn = Path(__file__).parent / "fixtures" / "day_01.txt"
    out = solve(fn, 3)
    assert out == ((3, 2, 4), 45000)
