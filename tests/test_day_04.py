from pathlib import Path

from src.day_04 import partial_overlap, solve


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_04.txt"
    out = solve(fn)
    assert out == 2


def test_partial_overlap():
    fn = Path(__file__).parent / "fixtures" / "day_04.txt"
    out = partial_overlap(fn)
    assert out == 4
