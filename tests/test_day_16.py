from pathlib import Path

import networkx as nx

from src.day_16 import solve2, total_pressure


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_16.txt"
    out = solve2(fn, 30)
    assert out == 1651


def test_total_pressure():
    optimal_data = [
        ("AA", {"status": 0, "flow": 0, "closed_at": 0}),
        ("BB", {"status": 1, "flow": 13, "closed_at": 25}),
        ("CC", {"status": 1, "flow": 2, "closed_at": 6}),
        ("DD", {"status": 1, "flow": 20, "closed_at": 28}),
        ("EE", {"status": 1, "flow": 3, "closed_at": 9}),
        ("FF", {"status": 0, "flow": 0, "closed_at": 0}),
        ("GG", {"status": 0, "flow": 0, "closed_at": 0}),
        ("HH", {"status": 1, "flow": 22, "closed_at": 13}),
        ("II", {"status": 0, "flow": 0, "closed_at": 0}),
        ("JJ", {"status": 1, "flow": 21, "closed_at": 21}),
    ]
    graph = nx.DiGraph()
    graph.add_nodes_from(optimal_data)
    assert total_pressure(graph) == 1651
