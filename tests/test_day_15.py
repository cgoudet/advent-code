from pathlib import Path

from src.day_15 import find_position, solve


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_15.txt"
    out = solve(fn, 10)
    assert out == 26


def test_find_frequency():
    fn = Path(__file__).parent / "fixtures" / "day_15.txt"
    out = find_position(fn, 20)
    assert out == 56000011
