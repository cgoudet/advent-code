from pathlib import Path

import numpy as np

from src.day_12 import authorised, find_trail, generate_graph, solve


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_12.txt"
    out = solve(fn)
    assert out == 31


def test_find_trail():
    fn = Path(__file__).parent / "fixtures" / "day_12.txt"
    out = find_trail(fn)
    assert out == 29


class TestAuthorised:
    def test_two_steps(self):
        elevations = np.arange(9).reshape(3, 3)
        assert not authorised(elevations, (0, 0), (0, 2))

    def test_move_too_far(self):
        elevations = np.arange(9).reshape(3, 3)
        assert not authorised(elevations, (2, 2), (2, 3))

    def test_move_too_steep(self):
        elevations = np.arange(9).reshape(3, 3)
        assert not authorised(elevations, (0, 0), (1, 0))

    def test_move_ok(self):
        elevations = np.arange(9).reshape(3, 3)
        assert authorised(elevations, (0, 0), (0, 1))


class TestGenerateGraph:
    def test_generation(self):
        elevations = np.arange(9).reshape(3, 3)
        graph = generate_graph(elevations)
        assert len(graph) == 9
        edges = sorted(graph.edges())
        expected = [
            ((0, 0), (0, 1)),
            ((0, 1), (0, 0)),
            ((0, 1), (0, 2)),
            ((0, 2), (0, 1)),
            ((1, 0), (0, 0)),
            ((1, 0), (1, 1)),
            ((1, 1), (0, 1)),
            ((1, 1), (1, 0)),
            ((1, 1), (1, 2)),
            ((1, 2), (0, 2)),
            ((1, 2), (1, 1)),
            ((2, 0), (1, 0)),
            ((2, 0), (2, 1)),
            ((2, 1), (1, 1)),
            ((2, 1), (2, 0)),
            ((2, 1), (2, 2)),
            ((2, 2), (1, 2)),
            ((2, 2), (2, 1)),
        ]
        assert edges == expected
