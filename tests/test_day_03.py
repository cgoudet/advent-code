from pathlib import Path

from src.day_03 import solve, solve_groups


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_03.txt"
    out = solve(fn)
    assert out == 157


def test_find_group_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_03.txt"
    out = solve_groups(fn)
    assert out == 70
