from pathlib import Path

from src.day_13 import Packet, find_divider, solve


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_13.txt"
    out = solve(fn)
    assert out == 13


class TestPacketComparator:
    def test_case_1(self):
        assert Packet([1, 1, 3, 1, 1]) <= Packet([1, 1, 5, 1, 1])

    def test_case_2(self):
        assert Packet([[1], [2, 3, 4]]) <= Packet([[1], 4])

    def test_case_3(self):
        assert not Packet([9]) <= Packet([[8, 7, 6]])

    def test_case_4(self):
        assert Packet([[4, 4], 4, 4]) <= Packet([[4, 4], 4, 4, 4])

    def test_case_5(self):
        assert not Packet([7, 7, 7, 7]) <= Packet([7, 7, 7])

    def test_case_6(self):
        assert Packet([]) <= Packet([3])

    def test_case_7(self):
        assert not Packet([[[]]]) <= Packet([[]])

    def test_case_8(self):
        assert not Packet([1, [2, [3, [4, [5, 6, 7]]]], 8, 9]) <= Packet(
            [1, [2, [3, [4, [5, 6, 0]]]], 8, 9]
        )


def test_divider():
    fn = Path(__file__).parent / "fixtures" / "day_13.txt"
    out = find_divider(fn)
    assert out == 140
