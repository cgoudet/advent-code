from pathlib import Path

from src.day_07 import (
    Directory,
    find_directory_to_delete,
    get_directory_structure,
    solve,
)


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_07.txt"
    out = solve(fn)
    assert out == 95437


def test_find_directory_to_delete():
    fn = Path(__file__).parent / "fixtures" / "day_07.txt"
    out = find_directory_to_delete(fn)
    assert out == 24933642


class TestDirectory:
    def test_update_content(self):
        state = Directory("name")
        content = ["dir a", "dir b", "123 test"]
        state.update_content(content)
        assert state.local_size == 123
        assert sorted(state.children.keys()) == ["a", "b"]
        assert state.children["a"].parent == state
        assert state.children["b"].parent == state


class TestExploratory:
    def test_from_input(self):
        fn = Path(__file__).parent / "fixtures" / "day_07.txt"
        out = get_directory_structure(fn)
        assert sorted(out.children.keys()) == ["a", "d"]
        assert sorted(out.children["a"].children.keys()) == ["e"]
