from pathlib import Path

from src.day_05 import solve, solve_new_crane


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_05.txt"
    out = solve(fn)
    assert out == "CMZ"


def test_find_new_crane():
    fn = Path(__file__).parent / "fixtures" / "day_05.txt"
    out = solve_new_crane(fn)
    assert out == "MCD"
