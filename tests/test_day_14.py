from pathlib import Path

import numpy as np

from src.day_14 import read_data, solve, solve_with_floor


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_14.txt"
    out = solve(fn)
    assert out == 24


def test_solve_with_floor():
    fn = Path(__file__).parent / "fixtures" / "day_14.txt"
    out = solve_with_floor(fn)
    assert out == 93


def test_read_data():
    fn = Path(__file__).parent / "fixtures" / "day_14.txt"
    out = read_data(fn)

    fn_map = Path(__file__).parent / "fixtures" / "day_14_map.txt"
    with fn_map.open() as f:
        expected_map = np.array(
            [list(x.strip()) for x in f.read().strip().split("\n")]
        ).astype(int)

    np.testing.assert_array_equal(out[:, 494:], expected_map)
