from pathlib import Path

from src.day_02 import solve, solve_inverse


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_02.txt"
    out = solve(fn)
    assert out == 15


def test_inverse_solve():
    fn = Path(__file__).parent / "fixtures" / "day_02.txt"
    out = solve_inverse(fn)
    assert out == 12
