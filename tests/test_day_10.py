from pathlib import Path

import numpy as np

from src.day_10 import image, read_data, solve, status_log


def test_status_log():
    instructions = ["noop", "addx 3", "addx -5"]
    out = status_log(instructions)
    assert out == [1, 1, 1, 4, 4, -1]


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_10.txt"
    out = solve(fn)
    assert out == 13140


def test_image():
    fn = Path(__file__).parent / "fixtures" / "day_10.txt"
    instructions = read_data(fn)
    logs = status_log(instructions)

    out = image(logs)

    fn = Path(__file__).parent / "fixtures" / "day_10_image.txt"
    with open(fn) as f:
        expected = [list(x) for x in f.read().strip().split("\n")]
    np.testing.assert_array_equal(out, expected)
