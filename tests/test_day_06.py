from pathlib import Path

from src.day_06 import solve


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_06.txt"
    out = solve(fn, 4)
    assert out == (7, 5, 6, 10, 11)


def test_more_char():
    fn = Path(__file__).parent / "fixtures" / "day_06.txt"
    out = solve(fn, 14)
    assert out == (19, 23, 23, 29, 26)
