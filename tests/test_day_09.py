from pathlib import Path

import numpy as np

from src.day_09 import format_moves, move_tail, solve


class TestMoveTail:
    def test_move_along_axis(self):
        np.testing.assert_array_equal(move_tail([2, 0], [0, 0]), [1, 0])

    def test_move_diagonal(self):
        np.testing.assert_array_equal(move_tail([2, 1], [0, 0]), [1, 1])

    def test_should_not_move_in_diagonal(self):
        np.testing.assert_array_equal(move_tail([1, 1], [0, 0]), [0, 0])


class TestFormatMoves:
    def test_correct_repeat(self):
        content = ["U 2", "L 3", "R 1"]
        out = format_moves(content)
        exp = np.array([(0, 1), (0, 1), (-1, 0), (-1, 0), (-1, 0), (1, 0)])
        np.testing.assert_array_equal(out, exp)


def test_find_correct_answer():
    fn = Path(__file__).parent / "fixtures" / "day_09.txt"
    out = solve(fn, 2)
    assert out == 13


def test_find_correct_answer_with_10_knots():
    fn = Path(__file__).parent / "fixtures" / "day_09.txt"
    out = solve(fn, 10)
    assert out == 1
