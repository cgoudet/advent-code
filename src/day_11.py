from collections import Counter
from pathlib import Path

import numpy as np


class Monkey:
    def __init__(
        self, items: list, op, argument: int, decision: tuple, divide: bool = True
    ):
        self.items = items
        self.operation = op
        self.argument = argument
        self.decision = decision
        self.inspected = 0
        self.divide = divide
        self.cnt = Counter()

    @classmethod
    def _define_operation(cls, content):
        if "old * old" in content:
            return ("square", 0)

        argument = int(content.strip().split(" ")[-1])
        return ("add" if "+" in content else "mul", argument)

    @classmethod
    def from_txt(cls, content, divide: bool):
        lines = [s.strip() for s in content.split("\n") if s.strip()]
        items = [int(x.strip()) for x in lines[1].split(":")[-1].split(",")]
        decision = tuple([int(x.strip().split(" ")[-1]) for x in lines[-3:]])
        op, argument = cls._define_operation(lines[2])
        return Monkey(items, op, argument, decision, divide)

    def update_worry(self, x):
        if isinstance(x, Item):
            return self.update_worry_item(x)
        return self.update_worry_int(x)

    def update_worry_item(self, x):
        if self.operation == "square":
            x = x.__sq__()
        elif self.operation == "add":
            x = x.__add__(self.argument)
        elif self.operation == "mul":
            x = x.__mul__(self.argument)
        if self.divide:
            x = x.__div__(3)
        return x

    def update_worry_int(self, x):
        if self.operation == "square":
            x = x * x
        elif self.operation == "add":
            x = x + self.argument
        elif self.operation == "mul":
            x = x * self.argument
        if self.divide:
            x = x // 3
        return x

    def receiver(self, worry):
        if isinstance(worry, Item):
            return (
                self.decision[2] if worry.rests[self.decision[0]] else self.decision[1]
            )
        else:
            return self.decision[2] if worry % self.decision[0] else self.decision[1]

    def round(self):
        self.initial = list(self.items)
        if not len(self.items):
            return []

        worries = [self.update_worry(x) for x in self.items]
        receivers = [self.receiver(x) for x in worries]
        self.inspected += len(worries)
        self.items = []
        self.cnt.update(receivers)
        return list(zip(worries, receivers))


class Item:
    def __init__(self, start, dividends):
        self.rests = {k: start % k for k in dividends}

    def __add__(self, x):
        self.rests = {k: (v + x) % k for k, v in self.rests.items()}
        return self

    def __mul__(self, x):
        self.rests = {k: v * x % k for k, v in self.rests.items()}
        return self

    def __sq__(self):
        self.rests = {k: v * v % k for k, v in self.rests.items()}
        return self


def read_data(fn):
    with open(fn) as f:
        return f.read().strip().split("\n\n")


def solve(fn, num_rounds: int, divide: bool):
    monkeys = [Monkey.from_txt(content, divide) for content in read_data(fn)]

    if divide is False:
        all_dividends = {m.decision[0] for m in monkeys}
        for monkey in monkeys:
            monkey.items = [Item(x, all_dividends) for x in monkey.items]

    for _ in range(num_rounds):
        for i, monkey in enumerate(monkeys):
            round = monkey.round()
            for worry, receiver in round:
                monkeys[receiver].items.append(worry)

    activities = sorted([(monkey.inspected, i) for i, monkey in enumerate(monkeys)])
    return np.prod([x[0] for x in activities[-2:]])


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_11.txt"
    print(solve(fn, 20, True))
    print(solve(fn, 10000, False))
