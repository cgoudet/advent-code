import functools
from pathlib import Path


def priority(item):
    code = ord(item)
    return code - 96 if code >= 97 else code + 27 - 65


def read_sacks(fn):
    with open(fn) as f:
        sacks = f.read().split("\n")
        return [x for x in sacks if x]


def solve(fn: Path) -> int:
    rugsacks = read_sacks(fn)

    total = 0
    for sack in rugsacks:
        N = len(sack) // 2
        error = list(set(sack[:N]) & set(sack[N:]))[0]
        total += priority(error)

    return total


def solve_groups(fn: Path) -> int:
    rugsacks = read_sacks(fn)

    total = 0
    for group in range(0, len(rugsacks), 3):
        common_items = functools.reduce(
            lambda a, b: set(a) & set(b), rugsacks[group : group + 3]
        )
        total += priority(list(common_items)[0])

    return total


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_03.txt"
    print(solve(fn))
    print(solve_groups(fn))
