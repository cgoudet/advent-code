from pathlib import Path

import pandas as pd


def solve(fn: Path, num_elves=1) -> tuple[tuple[int], int]:
    calories = (
        pd.read_csv(fn, header=None, names=["calories"], skip_blank_lines=False)
        .assign(elf=lambda df: df["calories"].isna().cumsum())
        .dropna(subset="calories")
        .astype(int)
        .groupby("elf")
        .agg({"calories": "sum"})
        .sort_values("calories", ascending=False)
        .iloc[:num_elves]
    )
    return tuple(calories.index.values), calories["calories"].sum()


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_01.txt"
    print(solve(fn))
    print(solve(fn, 3))
