from itertools import chain
from pathlib import Path

import numpy as np


def _read_coords(pattern):
    coords = pattern.strip().split("->")
    return [tuple([int(x) for x in coord.split(",")]) for coord in coords]


def read_data(fn):

    with open(fn) as f:
        patterns = [_read_coords(coord) for coord in f.read().strip().split("\n")]
    max_h, max_v = (max(serie) for serie in zip(*chain(*patterns)))

    topography = np.zeros(shape=(max_v + 1, max_h + 1))

    for vein in patterns:
        for start, end in zip(vein, vein[1:]):
            start_v = min(start[1], end[1])
            end_v = max(start[1], end[1]) + 1
            start_h = min(start[0], end[0])
            end_h = max(start[0], end[0]) + 1
            topography[start_v:end_v, start_h:end_h] = 1
    return topography


def fall(start, topography):
    """
    Modify topography to place the falling send to its final destination
    Return True if the sand ends up in empty space else False
    """
    (xs,) = np.where(topography[start[1] :, start[0]] > 0)
    if not len(xs):
        return topography
    if xs[0] > 1:
        return fall((start[0], start[1] + xs[0] - 1), topography)

    filler = (np.arange(len(topography)).reshape(-1, 1) + 1) // len(topography)
    if start[0] == topography.shape[1] - 1:
        topography = np.concatenate([topography, filler], axis=1)
    elif start[0] == 0:
        topography = np.concatenate([filler, topography], axis=1)
        start = (1, start[1])

    diag_left = (start[0] - 1, start[1] + 1)
    if topography[tuple(reversed(diag_left))] == 0:
        return fall(diag_left, topography)

    diag_right = (start[0] + 1, start[1] + 1)
    if topography[tuple(reversed(diag_right))] == 0:
        return fall(diag_right, topography)

    topography = np.array(topography)
    topography[tuple(reversed(start))] = 2
    return topography


def solve(fn):
    topography = read_data(fn)
    rocks = topography.sum()
    sand = 0
    while True:
        sand += 1
        topography = fall((500, 0), topography)
        if int(topography.sum()) < sand * 2 + rocks:
            break

    return sand - 1


def solve_with_floor(fn):
    topography = read_data(fn)
    topography = np.concatenate(
        [
            topography,
            np.zeros((1, topography.shape[1])),
            np.ones((1, topography.shape[1])),
        ]
    )
    topography[0, 500] = -1
    sand = 0
    start = (500, 0)
    while True:
        sand += 1
        topography = fall(start, topography)
        new_start = np.where(topography[0] == -1)[0]
        if not len(new_start):
            break
        start = (new_start[0], 0)

    return sand


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_14.txt"
    print(solve(fn))
    print(solve_with_floor(fn))
