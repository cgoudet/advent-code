import re
from pathlib import Path

import cvxpy as cp
import networkx as nx
import numpy as np


class Valve:
    def __init__(self, name, flow, to):
        self.name = name
        self.flow = flow
        self.to = to

    @classmethod
    def from_txt(cls, description):
        pat = re.match(
            "Valve ([A-Z]{2}) has flow rate=([0-9]{1,}); tunnels? leads? to valves? ([A-Z ,]{2,})",
            description,
        )

        to = pat.group(3).replace(" ", "").strip().split(",")
        return cls(pat.group(1), int(pat.group(2)), to)

    def __le__(self, other):
        return self.name <= other.name

    def __lt__(self, other):
        return self.name < other.name


def read_data(fn):
    with open(fn) as f:
        return [Valve.from_txt(desc) for desc in f.read().strip().split("\n")]


def generate_graph(valves):
    graph = nx.DiGraph()
    graph.add_nodes_from(
        [(v.name, {"status": 0, "flow": int(v.flow), "closed_at": 0}) for v in valves]
    )
    graph.add_edges_from([(v.name, n) for v in valves for n in v.to])
    return graph


def total_pressure(graph):
    return sum(
        [d["status"] * d["closed_at"] * d["flow"] for _, d in graph.nodes(data=True)]
    )


def explore(position, remaining, status, distances, flows):
    if remaining in [0, 1]:
        return status

    status = np.copy(status)
    if status[position] == 0:
        remaining -= 1
        status[position] = flows[position] * remaining

    possible_neighbours = np.where((status == 0) & (distances[position] <= remaining))[
        0
    ]
    if not len(possible_neighbours):
        return status
    to_compare = [
        explore(n, remaining - int(distances[position, n]), status, distances, flows)
        for n in possible_neighbours
    ]
    return sorted([(s.sum(), s) for s in to_compare], reverse=True, key=lambda x: x[0])[
        0
    ][1]


def solve2(fn, time):
    valves = sorted(read_data(fn))
    graph = generate_graph(valves)
    distances = pairwise_distance(graph)
    status = np.array([int(v.flow == 0) for v in valves])
    flows = np.array([int(v.flow) for v in valves])
    out = explore(0, time, status, distances, flows)

    removed_empty = out[flows > 0]
    return removed_empty.sum()


def pairwise_distance(graph):
    all_distances = list(nx.all_pairs_shortest_path_length(graph))
    order = sorted(graph.nodes)
    distances = np.zeros((len(order), len(order)))
    for name, others in all_distances:
        for neighbour, d in others.items():
            distances[order.index(name), order.index(neighbour)] = d
    return distances


def solve(fn, total_time):
    valves = read_data(fn)

    graph = generate_graph(valves)
    all_distances = list(nx.all_pairs_shortest_path_length(graph))
    order = sorted(graph.nodes)
    distances = np.zeros((len(order), len(order)))
    for name, others in all_distances:
        for neighbour, d in others.items():
            distances[order.index(name), order.index(neighbour)] = d

    variable = cp.Variable(shape=(total_time + 1, len(valves)), boolean=True)
    flow = np.array([[int(v.flow) for v in valves]])
    duration = np.arange(total_time, -1, -1).reshape(-1, 1)
    weight = flow * duration
    objective = cp.sum(cp.multiply(weight, variable)) - cp.sum(variable) / 1000
    constraints = [
        cp.sum(variable, axis=1) <= 1,
        cp.sum(variable, axis=0) <= 1,
        variable[0, 0] == 1,
    ]
    for i in range(len(order)):
        for j in range(0, len(order)):
            if i == j:
                continue
            for distance in range(1, int(distances[i][j]) + 1):
                constraints.append(
                    1 - variable[:-distance, i] >= variable[distance:, j]
                )
    problem = cp.Problem(cp.Maximize(objective), constraints)
    problem.solve()
    print(problem.status)
    return int((variable.value * weight).sum())


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_16.txt"
    print(solve2(fn, 30))
