import re
from collections import deque
from pathlib import Path


def read_data(fn):
    with open(fn) as f:
        lines = f.read().split("\n")
    sep_ind = lines.index("")
    stack = _format_stack(lines[:sep_ind])
    instructions = _format_instructions(lines[sep_ind + 1 :])
    return stack, instructions


def _format_instructions(instructions):
    pat = re.compile("move ([0-9]{1,}) from ([0-9]{1,}) to ([0-9]{1,})")
    return [[int(x) for x in pat.match(inst).groups()] for inst in instructions if inst]


def _format_stack(stack):
    indices = []
    for i, c in enumerate(stack[-2]):
        if c == "[":
            indices.append(i + 1)

    towers = [deque() for _ in indices]
    for height in range(len(stack) - 1):
        level = stack[-2 - height]
        for index, tower in zip(indices, towers):
            to_add = level[index]
            if to_add != " ":
                tower.append(to_add)
    return towers


def solve(fn: Path) -> int:
    stack, instructions = read_data(fn)
    for repeat, start, end in instructions:
        for _ in range(repeat):
            stack[end - 1].append(stack[start - 1].pop())
    return "".join([s.pop() for s in stack])


def solve_new_crane(fn: Path) -> int:
    stack, instructions = read_data(fn)
    for repeat, start, end in instructions:
        to_add = [stack[start - 1].pop() for _ in range(repeat)]
        for crate in to_add[::-1]:
            stack[end - 1].append(crate)
    return "".join([s.pop() for s in stack])


if __name__ == "__main__":

    fn = Path(__file__).parents[1] / "data" / "day_05.txt"
    print(solve(fn))
    print(solve_new_crane(fn))
