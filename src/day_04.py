from io import StringIO
from pathlib import Path

import numpy as np
import pandas as pd


def read_data(fn):
    with open(fn) as f:
        content = StringIO(f.read().replace("-", ","))
        return pd.read_csv(
            content, header=None, names=["start0", "end0", "start1", "end1"]
        )


def _complete_inclusion(pairs: pd.DataFrame) -> pd.DataFrame:
    same_edges = (pairs["start0"] == pairs["start1"]) | (pairs["end0"] == pairs["end1"])
    diff_edges = np.where(
        pairs["start0"] > pairs["start1"],
        pairs["end0"] < pairs["end1"],
        pairs["end0"] > pairs["end1"],
    )
    return same_edges | diff_edges


def solve(fn: Path) -> int:
    pairs = read_data(fn)
    return _complete_inclusion(pairs).sum()


def partial_overlap(fn: Path) -> int:
    pairs = read_data(fn)
    mask = (
        _complete_inclusion(pairs)
        | ((pairs["start0"] <= pairs["end1"]) & (pairs["start0"] > pairs["start1"]))
        | ((pairs["start1"] <= pairs["end0"]) & (pairs["start1"] > pairs["start0"]))
    )
    return mask.sum()


if __name__ == "__main__":

    fn = Path(__file__).parents[1] / "data" / "day_04.txt"
    print(solve(fn))
    print(partial_overlap(fn))
