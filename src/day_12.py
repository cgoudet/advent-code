import itertools
from pathlib import Path

import networkx as nx
import numpy as np

from src.day_03 import priority

vect_prio = np.vectorize(priority)


def authorised(elevations, start, end):
    start, end = np.array(start), np.array(end)
    if (
        end.min() < 0
        or (end >= np.array(elevations.shape)).any()
        or np.abs(end - start).max() > 1
        or elevations[tuple(start)] - elevations[tuple(end)] < -1
    ):
        return False
    return True


def read_data(fn: Path) -> np.array:
    with fn.open() as f:
        raw_data = np.array([list(x.strip()) for x in f.read().strip().split("\n")])
    start = tuple(np.array(np.where(raw_data == "S")).flatten())
    end = tuple(np.array(np.where(raw_data == "E")).flatten())
    elevations = vect_prio(raw_data)

    elevations[start] = 1
    elevations[end] = 26
    return elevations, start, end


def generate_graph(elevations):
    G = nx.DiGraph(directed=True)
    nodes = list(
        itertools.product(range(elevations.shape[0]), range(elevations.shape[1]))
    )
    G.add_nodes_from(nodes)
    for node in nodes:
        for move in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
            neighbor = tuple(np.array(node) + np.array(move))
            if authorised(elevations, node, neighbor):
                G.add_edge(node, neighbor)
    return G


def solve(fn):
    elevations, start, end = read_data(fn)
    graph = generate_graph(elevations)
    return nx.shortest_path_length(graph, start, end)


def find_trail(fn):
    elevations, start, end = read_data(fn)
    graph = generate_graph(elevations)
    possible_starts = list(zip(*np.where(elevations == 1)))

    acceptable = []
    for start in possible_starts:
        try:
            path = nx.shortest_path_length(graph, start, end)
            acceptable.append(path)
        except nx.NetworkXNoPath:
            pass

    return min(acceptable)


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_12.txt"
    print(solve(fn))
    print(find_trail(fn))
