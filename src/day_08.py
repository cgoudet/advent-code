from pathlib import Path

import numpy as np
import pandas as pd


def read_heights(fn):
    with open(fn) as f:
        heights = [list(x) for x in f.read().split() if x]
    return pd.DataFrame(heights).astype(int)


def is_seen(df):
    return df.cummax().shift(1).fillna(df - 1) < df


def vision(heights):
    from_left = is_seen(heights.T).T
    from_right = is_seen(heights.T[::-1])[::-1].T
    from_top = is_seen(heights)
    from_bottom = is_seen(heights[::-1])[::-1]
    return from_left | from_right | from_top | from_bottom


def solve(fn: Path) -> int:
    heights = read_heights(fn)
    is_tree_seen = vision(heights)
    return is_tree_seen.values.sum()


def max_view(x):
    if len(x) == 1:
        return 0
    blocked = x[:-1] >= x[-1]
    where_blocked = np.where(blocked[::-1])
    if len(where_blocked[0]):
        return where_blocked[0][0] + 1
    return len(x) - 1


def view_size(df):
    return df.expanding().apply(max_view, raw=True)


def scenic_view(fn):
    heights = read_heights(fn)
    from_left = view_size(heights.T).T
    from_right = view_size(heights.T[::-1])[::-1].T
    from_top = view_size(heights)
    from_bottom = view_size(heights[::-1])[::-1]
    all_views = (from_left * from_right * from_top * from_bottom).astype(int)
    return all_views.values.max()


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_08.txt"
    print(solve(fn))
    print(scenic_view(fn))
