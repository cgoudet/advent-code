import itertools
from pathlib import Path

import numpy as np


def move_tail(head_pos, tail_pos):
    head_pos, tail_pos = np.array(head_pos), np.array(tail_pos)
    if np.max(np.abs(head_pos - tail_pos)) <= 1:
        return tail_pos
    direction = np.round((head_pos - tail_pos) * 0.55).astype(int)
    return tail_pos + direction


def format_moves(content):
    direction_match = {"R": (1, 0), "U": (0, 1), "L": (-1, 0), "D": (0, -1)}
    directions, repeats = zip(*[line.split(" ") for line in content if line])
    directions = np.array([direction_match[x] for x in directions]).astype(int)
    repeats = np.array(repeats).astype(int)
    return np.repeat(directions, repeats, axis=0)


def solve(fn, knots):
    knots_pos = np.zeros(shape=(knots, 2))
    with open(fn) as f:
        all_moves = format_moves(f.read().split("\n"))

    all_tail_pos = []
    for move in all_moves:
        knots_pos[0] = knots_pos[0] + move
        knots_pos = np.array(list(itertools.accumulate(knots_pos, move_tail)))
        all_tail_pos.append(tuple(knots_pos[-1]))
    return len(set(all_tail_pos))


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_09.txt"
    print(solve(fn, 2))
    print(solve(fn, 10))
