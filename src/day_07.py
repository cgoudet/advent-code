from pathlib import Path


class Directory:
    local_size = 0

    def __init__(self, name, parent=None):
        self.name = name
        self.parent = parent
        self.children = {}

    def update_content(self, content):
        content = [x for x in content if x]
        for c in content:
            dir_or_size, name = c.strip().split(" ")
            if dir_or_size == "dir":
                self.children[name] = Directory(name, parent=self)
            else:
                self.local_size += int(dir_or_size)

    def substructure_sizes(self):
        combined = {}
        for _, child in self.children.items():
            combined |= {
                Path(self.name) / k: v for k, v in child.substructure_sizes().items()
            }
        total = (
            sum([combined[Path(self.name) / name] for name in self.children])
            + self.local_size
        )
        return {self.name: total, **combined}


class Explorator:
    def __init__(self):
        self.tree_ = Directory("/")
        self.current = self.tree_

    def parse_command(self, command):
        content = command.split("\n")
        prompt = content[0].strip()
        if prompt == "cd ..":
            self.current = self.current.parent
        elif prompt.startswith("cd "):
            name = prompt[3:]
            self.current = self.current.children[name]
        elif prompt.startswith("ls"):
            self.current.update_content(content[1:])


def get_directory_structure(fn: Path):
    with fn.open() as f:
        commands = f.read().split("$")[2:]

    explo = Explorator()
    [explo.parse_command(command) for command in commands if command]
    return explo.tree_


def solve(fn: Path) -> int:
    tree = get_directory_structure(fn)
    sizes = tree.substructure_sizes()
    return sum([v for _, v in sizes.items() if v < 100000])


def find_directory_to_delete(fn: Path) -> int:
    tree = get_directory_structure(fn)
    sizes = tree.substructure_sizes()
    to_free = 30_000_000 - (70_000_000 - sizes["/"])
    return sorted([(v, k) for k, v in sizes.items() if v > to_free])[0][0]


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_07.txt"
    print(solve(fn))
    print(find_directory_to_delete(fn))
