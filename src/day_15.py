import re
from pathlib import Path

import numpy as np
from scipy.spatial.distance import cdist


def read_data(fn):
    pat = re.compile(
        "Sensor at x=(-?[0-9]{1,}), y=(-?[0-9]{1,}): closest beacon is at x=(-?[0-9]{1,}), y=(-?[0-9]{1,})"
    )

    with open(fn) as f:
        raw = f.read().strip().split("\n")
        extracts = [pat.match(x) for x in raw]
    positions = [[int(m.group(i)) for i in range(1, 5)] for m in extracts]
    xs, ys, xb, yb = zip(*positions)
    sensors = np.array([xs, ys]).T
    beacons = np.array([xb, yb]).T
    return sensors, beacons


def solve(fn, row):
    sensors, beacons = read_data(fn)
    distances = np.sum(np.abs(sensors - beacons), axis=1)
    min_row = (sensors[:, 0] - distances).min()
    max_row = (sensors[:, 0] + distances).max()
    asked_row = np.vstack(
        [np.arange(min_row, max_row + 1), np.ones(max_row - min_row + 1) * row]
    ).T
    out = cdist(asked_row, sensors, metric="cityblock")
    covered = (out <= distances.reshape(1, -1)).astype(int).max(axis=1)
    already_present = len(np.where(beacons[:, 0] == row))
    return covered.sum() - already_present


def find_position(fn, shape):
    sensors, beacons = read_data(fn)
    distances = np.sum(np.abs(sensors - beacons), axis=1)
    topography = np.zeros((shape, shape), dtype=np.int8)
    for (x, y), d in zip(sensors, distances):
        for step in range(d):
            topography[
                max(x - step, 0) : x + step + 1,
                max(y - d + step, 0) : max(0, y + d - step) + 1,
            ] = 1
    pos = np.where(topography == 0)
    return pos[0][0] * 4000000 + pos[1][0]


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_15.txt"
    print(solve(fn, 2000000))
    print(find_position(fn, 4000000))
