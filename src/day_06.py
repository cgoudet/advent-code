from pathlib import Path

import pandas as pd


def read_data(fn):
    with open(fn) as f:
        return [x for x in f.read().split("\n") if x]


def _solve_single(line, num_char):
    out = (
        pd.pivot_table(
            pd.DataFrame({"letter": list(line)}).reset_index().assign(one=1),
            index="index",
            columns="letter",
            values="one",
            fill_value=0,
        )
        .rolling(num_char, min_periods=1)
        .sum()
        .max(axis=1)
        .pipe(lambda s: s[(s.index.values >= num_char - 1) & (s == 1)])
        .index[0]
        + 1
    )
    return out


def solve(fn: Path, num_char) -> int:
    lines = read_data(fn)
    return tuple(_solve_single(line, num_char) for line in lines)


if __name__ == "__main__":

    fn = Path(__file__).parents[1] / "data" / "day_06.txt"
    print(solve(fn, 4))
    print(solve(fn, 14))
