from pathlib import Path

import numpy as np
import pandas as pd

K = 2 * np.pi / 3
POSITIONS = {move: pos for move, pos in zip("ABC", np.exp(np.arange(3) * K * 1j))}


def read_parchment(fn: Path) -> pd.DataFrame:
    return pd.read_csv(fn, header=None, names=["other", "me"], sep=" ")


def fill_simple_match(df: pd.DataFrame) -> pd.DataFrame:
    match = {k: v for k, v in zip("XYZ", "ABC")}
    return df.assign(me=lambda df: df["me"].map(match))


def score_rounds(df: pd.DataFrame) -> int:
    shape_scores = {k: i + 1 for i, k in enumerate("ABC")}

    rounds = df.assign(
        shape_score=lambda df: df["me"].map(shape_scores),
        me_pos=lambda df: df["me"].map(POSITIONS),
        other_pos=lambda df: df["other"].map(POSITIONS),
        is_win=lambda df: np.angle(df["me_pos"] * np.conjugate(df["other_pos"])) / K,
        win_score=lambda df: 3 * (df["is_win"] + 1),
        round_score=lambda df: df["win_score"] + df["shape_score"],
    )
    return np.round(rounds["round_score"]).astype(int).sum()


def solve(fn: Path) -> tuple[tuple[int], int]:
    return read_parchment(fn).pipe(fill_simple_match).pipe(score_rounds)


def fill_from_result(df: pd.DataFrame) -> pd.DataFrame:
    indic_to_angle = {"X": -K, "Y": 0, "Z": K}
    angle_to_move = {int(np.angle(pos) / K): move for move, pos in POSITIONS.items()}
    return df.assign(
        other_pos=lambda df: df["other"].map(POSITIONS),
        angle=lambda df: df["me"].map(indic_to_angle),
        me_angle=lambda df: np.round(
            np.angle(df["other_pos"] * np.exp(df["angle"] * 1j)) / K
        ).astype(int),
        me=lambda df: df["me_angle"].map(angle_to_move),
    ).drop(columns=["other_pos", "angle", "me_angle"])


def solve_inverse(fn: Path) -> tuple[tuple[int], int]:
    return read_parchment(fn).pipe(fill_from_result).pipe(score_rounds)


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_02.txt"
    print(solve(fn))
    print(solve_inverse(fn))
