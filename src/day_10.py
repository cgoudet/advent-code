from pathlib import Path

import numpy as np


def read_data(fn):
    with open(fn) as f:
        instructions = f.read().strip().split("\n")
    return instructions


def status_log(instructions):
    status = [1]
    for line in instructions:
        if line == "noop":
            status.append(status[-1])
        else:
            increment = int(line.split(" ")[1])
            status += [status[-1], status[-1] + increment]
    return status


def image(logs):
    crt_pos = np.arange(40).reshape(1, 40)
    pixel_log = np.array(logs[:-1]).reshape(6, 40)
    return np.where(np.abs(crt_pos - pixel_log) <= 1, "#", ".")


def solve(fn):
    instructions = read_data(fn)
    logs = status_log(instructions)

    scores = np.array([x for x in zip(logs, np.arange(len(logs)) + 1)])
    print("\n".join(["".join(x) for x in image(logs)]))
    return scores.prod(axis=1)[19::40].sum()


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_10.txt"
    print(solve(fn))
