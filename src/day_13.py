from itertools import chain, zip_longest
from pathlib import Path

import numpy as np


def _compare_packets(first, second):
    if isinstance(first, int) and isinstance(second, int):
        return 0 if first == second else 2 * (first < second) - 1

    if isinstance(first, int) or isinstance(second, int):
        first, second = (
            ([first], second) if isinstance(first, int) else (first, [second])
        )

    if isinstance(first, list) and isinstance(second, list):
        for fel, sel in zip_longest(first, second):
            if fel is None or sel is None:
                return (fel is None) - (sel is None)
            status = _compare_packets(fel, sel)
            if status != 0:
                return status
    return 0


class Packet(list):
    def __le__(self, second):
        status = _compare_packets(self, second)
        return status >= 0

    def __lt__(self, second):
        status = _compare_packets(self, second)
        return status > 0

    def __eq__(self, second):
        status = _compare_packets(self, second)
        return status == 0


def read_data(fn):
    with open(fn) as f:
        return [
            [Packet(eval(packet)) for packet in x.strip().split("\n")]
            for x in f.read().strip().split("\n\n")
        ]


def solve(fn):
    packets = read_data(fn)
    return sum([i for i, x in enumerate(packets, start=1) if x[0] <= x[1]])


def find_divider(fn):
    dividers = [Packet([[6]]), Packet([[2]])]
    packets = sorted(list(chain(*read_data(fn))) + dividers)
    return np.product([i + 1 for i, pack in enumerate(packets) if pack in dividers])


if __name__ == "__main__":
    fn = Path(__file__).parents[1] / "data" / "day_13.txt"
    print(solve(fn))
    print(find_divider(fn))
